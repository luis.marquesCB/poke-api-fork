from django.contrib import admin
from .models import Trainer, Pokemons


# Register your models here.


class TrainerAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "age", "get_team")
    filter_horizontal = ("team",)
    readonly_fields = ("image_tag",)

    def get_team(self, obj):
        return [pokemon.name for pokemon in obj.team.all()]


class PokemonsAdmin(admin.ModelAdmin):
    list_display = (
        "id_pokemon",
        "name",
        "type",
        "experience",
        "weight",
        "picture",
    )
    readonly_fields = ("image_tag",)

    def get_ordering(self, request):
        return ["id_pokemon"]


admin.site.register(Trainer, TrainerAdmin)
admin.site.register(Pokemons, PokemonsAdmin)
