from poke.repositories.pokemon import PokemonRepository


class DetailPokemonUseCase:
    """
    Use case for retrieving details of a Pokemon.

    Args:
        repository (PokemonRepository): Repository for accessing Pokemon data.

    Attributes:
        repository (PokemonRepository): Repository for accessing Pokemon data.
    """

    repository = PokemonRepository()

    def __init__(self, repository=None):
        """
        Initializes the DetailPokemonUseCase.

        Args:
            repository (PokemonRepository): Repository for accessing Pokemon data.
        """
        self.repository = repository or self.repository

    def execute(self, data: dict):
        """
        Executes the use case to retrieve details of a Pokemon.

        Args:
            data (dict): Data required to retrieve Pokemon details.

        Returns:
            dict: Details of the requested Pokemon.
        """
        return self.repository.get(data)


class ListPokemonUseCase:
    """
    Use case for listing Pokemon.

    Args:
        repository (PokemonRepository): Repository for accessing Pokemon data.

    Attributes:
        repository (PokemonRepository): Repository for accessing Pokemon data.
    """

    repository = PokemonRepository()

    def __init__(self, repository=None):
        """
        Initializes the ListPokemonUseCase.

        Args:
            repository (PokemonRepository): Repository for accessing Pokemon data.
        """
        self.repository = repository or self.repository

    def execute(self, data: dict):
        """
        Executes the use case to list Pokemon.

        Args:
            data (dict): Data required to list Pokemon.

        Returns:
            list: List of Pokemon.
        """
        return self.repository.list(data)
