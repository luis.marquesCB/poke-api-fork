from poke.repositories.trainer import TrainerRepository, TeamRepository


class CreateTrainerUseCase:
    """
    Use case responsible for creating a new trainer.

    Args:
        trainer_repository (TrainerRepository): Repository responsible for handling trainer data.

    Methods:
        execute(data: dict): Executes the use case, creating a new trainer with the given data.
    """

    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        """
        Executes the use case, creating a new trainer with the given data.

        Args:
            data (dict): Dictionary containing the data for the new trainer.

        Returns:
            dict: Dictionary containing the data for the newly created trainer.
        """
        return self.trainer_repository.create(data)


class DetailTrainerUseCase:
    """
    Use case responsible for retrieving the details of a trainer.

    Args:
        trainer_repository (TrainerRepository): Repository responsible for handling trainer data.

    Methods:
        execute(data: dict): Executes the use case, retrieving the details of the trainer with the given data.
    """

    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        """
        Executes the use case, retrieving the details of the trainer with the given data.

        Args:
            data (dict): Dictionary containing the data for the trainer to retrieve.

        Returns:
            dict: Dictionary containing the data for the retrieved trainer.
        """
        return self.trainer_repository.get_detail(data)


class UpdateTrainerUseCase:
    """
    Use case responsible for updating a trainer.

    Args:
        trainer_repository (TrainerRepository): Repository responsible for handling trainer data.

    Methods:
        execute(data: dict): Executes the use case, updating the trainer with the given data.
    """

    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        """
        Executes the use case, updating the trainer with the given data.

        Args:
            data (dict): Dictionary containing the data for the trainer to update.

        Returns:
            dict: Dictionary containing the data for the updated trainer.
        """
        return self.trainer_repository.update(data)


class DeleteTrainerUseCase:
    """
    Use case responsible for deleting a trainer.

    Args:
        trainer_repository (TrainerRepository): Repository responsible for handling trainer data.

    Methods:
        execute(data: dict): Executes the use case, deleting the trainer with the given data.
    """

    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        """
        Executes the use case, deleting the trainer with the given data.

        Args:
            data (dict): Dictionary containing the data for the trainer to delete.

        Returns:
            dict: Dictionary containing the data for the deleted trainer.
        """
        return self.trainer_repository.delete(data)


class ListTrainerUseCase:
    """
    Use case responsible for listing trainers.

    Args:
        trainer_repository (TrainerRepository): Repository responsible for handling trainer data.

    Methods:
        execute(data: dict): Executes the use case, listing trainers with the given data.
    """

    trainer_repository = TrainerRepository()

    def __init__(self, trainer_repository=None):
        self.trainer_repository = trainer_repository or self.trainer_repository

    def execute(self, data: dict):
        """
        Executes the use case, listing trainers with the given data.

        Args:
            data (dict): Dictionary containing the data for the trainers to list.

        Returns:
            list: List containing the data for the listed trainers.
        """
        return self.trainer_repository.list(data)


class UpdateTrainerTeamUseCase:
    """
    Use case responsible for updating a trainer's team.

    Args:
        team_repository (TeamRepository): Repository responsible for handling team data.

    Methods:
        execute(data: dict): Executes the use case, updating the team with the given data.
    """

    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        """
        Executes the use case, updating the team with the given data.

        Args:
            data (dict): Dictionary containing the data for the team to update.

        Returns:
            dict: Dictionary containing the data for the updated team.
        """
        return self.team_repository.update_team(data)


class CreateTrainerTeamUseCase:
    """
    Use case responsible for creating a trainer's team.

    Args:
        team_repository (TeamRepository): Repository responsible for handling team data.

    Methods:
        execute(data: dict): Executes the use case, creating a new team with the given data.
    """

    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        """
        Executes the use case, creating a new team with the given data.

        Args:
            data (dict): Dictionary containing the data for the new team.

        Returns:
            dict: Dictionary containing the data for the newly created team.
        """
        return self.team_repository.create_team(data)


class GetTrainerTeamUseCase:
    """
    Use case responsible for retrieving a trainer's team.

    Args:
        team_repository (TeamRepository): Repository responsible for handling team data.

    Methods:
        execute(data: dict): Executes the use case, retrieving the team with the given data.
    """

    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        """
        Executes the use case, retrieving the team with the given data.

        Args:
            data (dict): Dictionary containing the data for the team to retrieve.

        Returns:
            dict: Dictionary containing the data for the retrieved team.
        """
        return self.team_repository.get_team(data)


class DeleteTrainerTeamUseCase:
    """
    Use case responsible for deleting a trainer's team.

    Args:
        team_repository (TeamRepository): Repository responsible for handling team data.

    Methods:
        execute(data: dict): Executes the use case, deleting the team with the given data.
    """

    team_repository = TeamRepository()

    def __init__(self, team_repository=None):
        self.team_repository = team_repository or self.team_repository

    def execute(self, data: dict):
        """
        Executes the use case, deleting the team with the given data.

        Args:
            data (dict): Dictionary containing the data for the team to delete.

        Returns:
            dict: Dictionary containing the data for the deleted team.
        """
        return self.team_repository.delete_team(data)
