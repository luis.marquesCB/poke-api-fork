from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.pagination import LimitOffsetPagination
from django.shortcuts import get_object_or_404
from django.db.models import Q
from poke.repositories.trainer import TrainerRepository
from poke.repositories.pokemon import PokemonRepository
from django.http import (
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
    HttpResponseServerError,
)
from django.template import Context, Engine, TemplateDoesNotExist, loader
from django.views.decorators.csrf import requires_csrf_token
from poke.serializers.pokemon import (
    DetailPokemonSerializer,
    ListPokemonSerializer,
    RetrieverPokemonSerializer,
)
from poke.serializers.trainer import (
    CreateTrainerSerializer,
    TrainerSerializer,
    ListTrainerSerializer,
    CreateTrainerTeamSerializer,
    TrainerTeamSerializer,
)
from poke.use_cases.pokemon import DetailPokemonUseCase, ListPokemonUseCase
from poke.use_cases.trainer import (
    CreateTrainerUseCase,
    DetailTrainerUseCase,
    ListTrainerUseCase,
    UpdateTrainerUseCase,
    DeleteTrainerUseCase,
    UpdateTrainerTeamUseCase,
    DeleteTrainerTeamUseCase,
    CreateTrainerTeamUseCase,
    GetTrainerTeamUseCase,
)
from drf_yasg.utils import swagger_auto_schema


class CustomPagination(LimitOffsetPagination):
    default_limit = 10
    page_query_param = "page"


class DetailPokemonAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = DetailPokemonSerializer
    use_case = DetailPokemonUseCase()

    def __init__(self, permission=None, serializer=None, use_case=None):
        self.permission_classes = permission or self.permission_classes
        self.serializer_class = serializer or self.serializer_class
        self.use_case = use_case or self.use_case

    @swagger_auto_schema(responses={200: DetailPokemonSerializer})
    def get(self, request, name: str):
        body = {**request.GET.dict(), "name": name.lower()}
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(
            self.use_case.execute(serializer.data), context=context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)


class ListPokemonAPIView(APIView, CustomPagination):
    permission_classes = (AllowAny,)
    serializer_class = ListPokemonSerializer
    use_case = ListPokemonUseCase()

    def __init__(self, permission=None, serializer=None, use_case=None):
        self.permission_classes = permission or self.permission_classes
        self.serializer_class = serializer or self.serializer_class
        self.use_case = use_case or self.use_case

    @swagger_auto_schema(responses={200: RetrieverPokemonSerializer})
    def get(self, request):
        body = request.GET.copy()
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        data = serializer.data.copy()

        response = RetrieverPokemonSerializer(
            self.paginate_queryset(self.use_case.execute(data), request, view=self),
            many=True,
            context=context,
        ).data

        return self.get_paginated_response(response)


class TrainerUserAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = TrainerSerializer
    detail_use_case = DetailTrainerUseCase()
    delete_use_case = DeleteTrainerUseCase()
    update_use_case = UpdateTrainerUseCase()

    def __init__(
        self,
        serializer=None,
        permission=None,
        detail_use_case=None,
        delete_use_case=None,
        update_use_case=None,
    ) -> None:
        self.permission_classes = permission or self.permission_classes
        self.serializer_class = serializer or self.serializer_class
        self.delete_use_case = delete_use_case or self.delete_use_case
        self.update_use_case = update_use_case or self.update_use_case
        self.detail_use_case = detail_use_case or self.detail_use_case

    @swagger_auto_schema(responses={200: TrainerSerializer})
    def get(self, request, id):
        body = {**request.GET.dict(), "id": id}
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(
            self.detail_use_case.execute(serializer.data), context=context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: TrainerSerializer})
    def put(self, request, id):
        body = request.data.copy()
        body["id"] = id
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            print(serializer.data)
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(
            self.update_use_case.execute(serializer.data), context=context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema
    def delete(self, request, id):
        body = {**request.GET.dict(), "id": id}
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        response = self.delete_use_case.execute(serializer.data)

        return Response(response, status=status.HTTP_200_OK)


class TrainerAPIView(APIView, CustomPagination):
    permission_classes = (AllowAny,)
    create_trainer_use_case = CreateTrainerUseCase()
    serializer_class = CreateTrainerSerializer
    use_case = ListTrainerUseCase()

    def __init__(
        self,
        permission=None,
        create_trainer_use_case=None,
        serializer=None,
        use_case=None,
    ) -> None:
        self.permission_classes = permission or self.permission_classes
        self.create_trainer_use_case = (
            create_trainer_use_case or self.create_trainer_use_case
        )
        self.serializer_class = serializer or self.serializer_class
        self.use_case = use_case or self.use_case

    @swagger_auto_schema(responses={200: TrainerSerializer})
    def post(self, request):
        body = request.data.copy()
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        response = self.create_trainer_use_case.execute(serializer.data)
        return Response(response, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: TrainerSerializer})
    def get(self, request):
        body = request.GET.copy()
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        data = serializer.data.copy()

        response = TrainerSerializer(
            self.paginate_queryset(self.use_case.execute(data), request, view=self),
            many=True,
            context=context,
        ).data

        return self.get_paginated_response(response)


class TeamAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = TrainerTeamSerializer
    get_team_use_case = GetTrainerTeamUseCase()
    delete_team_use_case = DeleteTrainerTeamUseCase()
    update_team_use_case = UpdateTrainerTeamUseCase()

    def __init__(
        self,
        permission=None,
        serializer=None,
        get_team_use_case=None,
        delete_team_use_case=None,
        update_team_use_case=None,
    ):
        self.permission_classes = permission or self.permission_classes
        self.serializer_class = serializer or self.serializer_class
        self.get_team_use_case = get_team_use_case or self.get_team_use_case
        self.delete_team_use_case = delete_team_use_case or self.delete_team_use_case
        self.update_team_use_case = update_team_use_case or self.update_team_use_case

    @swagger_auto_schema(responses={200: TrainerTeamSerializer})
    def get(self, request, id):
        body = {**request.GET.dict(), "id": id}
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(
            self.get_team_use_case.execute(serializer.data), context=context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: TrainerTeamSerializer})
    def put(self, request, id):
        body = request.data.copy()
        body["id"] = id
        print(body)
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            print(serializer.data)
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(
            self.update_team_use_case.execute(serializer.data), context=context
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema
    def delete(self, request, id):
        body = {**request.GET.dict(), "id": id}
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        response = self.delete_team_use_case.execute(serializer.data)

        return Response(response, status=status.HTTP_200_OK)


class CreateTeamAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateTrainerTeamSerializer
    use_case = CreateTrainerTeamUseCase()

    def __init__(
        self,
        serializer=None,
        permission=None,
        use_case=None,
    ) -> None:
        self.permission_classes = permission or self.permission_classes
        self.serializer_class = serializer or self.serializer_class
        self.use_case = use_case or self.use_case

    @swagger_auto_schema(responses={200: TrainerTeamSerializer})
    def post(self, request):
        body = request.data.copy()
        context = {"request": request}
        serializer = self.serializer_class(data=body, context=context)

        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        response = self.use_case.execute(serializer.data)

        return Response(response, status=status.HTTP_200_OK)
