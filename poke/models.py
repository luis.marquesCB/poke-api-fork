from django.db import models
from django.utils.html import mark_safe


class Pokemons(models.Model):
    id_pokemon = models.IntegerField(unique=True)
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    experience = models.IntegerField(blank=True, null=True)
    weight = models.IntegerField()
    picture = models.URLField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.picture))

    image_tag.short_description = "Image"
    image_tag.allow_tags = True

    class Meta:
        verbose_name = "Pokemon"
        verbose_name_plural = "Pokemons"


class Trainer(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()
    picture = models.URLField(max_length=255, blank=True, null=True)
    team = models.ManyToManyField(Pokemons, blank=True)

    def __str__(self):
        return self.name

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.picture))

    image_tag.short_description = "Image"
    image_tag.allow_tags = True

    class Meta:
        verbose_name = "Trainer"
        verbose_name_plural = "Trainers"
