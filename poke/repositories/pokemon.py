from poke.models import Pokemons
from coreapi.exceptions import AppError
from django.db.models import Q
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist


class PokemonRepository:
    def get(self, data: dict):
        try:
            name = data.get("name", "")
            pokemon = Pokemons.objects.get(name__exact=name)

            if pokemon:
                return {
                    "id_pokemon": pokemon.id_pokemon,
                    "name": pokemon.name,
                    "type": pokemon.type,
                    "experience": pokemon.experience,
                    "weight": pokemon.weight,
                    "picture": pokemon.picture,
                }
        except Exception as e:
            message = f"Pokemon {name} Not Found: {e}"
            raise AppError(message=message)

    def list(self, data: dict):
        try:
            return list(Pokemons.objects.all().values("id_pokemon", "name", "type"))
        except Exception as e:
            message = f"Error getting pokemons list: {e}"
            raise AppError(message=message)
