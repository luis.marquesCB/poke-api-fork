from coreapi.exceptions import AppError
from poke.models import Trainer, Pokemons


class TrainerRepository:
    def create(self, data: dict):
        try:
            Trainer.objects.create(**data)
            return {"message": "Trainer has been created with sucess"}
        except Exception as e:
            message = f"Error creating trainer: {e}"
            raise AppError(message=message)

    def get_detail(self, data):
        try:
            trainer = Trainer.objects.get(id=data["id"])
            result = {
                "id": trainer.id,
                "name": trainer.name,
                "age": trainer.age,
                "team": [pokemon for pokemon in trainer.team.all().values()],
            }
            return result
        except Exception as e:
            message = f"Error getting trainer detail: {e}"
            raise AppError(message=message)

    def update(self, data: dict):
        try:
            id = data.pop("id")
            trainer = Trainer.objects.get(id=id)
            for key, value in data.items():
                setattr(trainer, key, value)
            trainer.save()
            return {
                "name": trainer.name,
                "age": trainer.age,
                "picture": trainer.picture,
            }
        except Exception as e:
            message = f"Error updating trainer: {e}"
            raise AppError(message=message)

    def delete(self, data: dict):
        try:
            trainer = Trainer.objects.get(id=data["id"])
            trainer.delete()
            return {"message": "Trainer has been deleted with success"}
        except Exception as e:
            message = f"Error deleting trainer: {e}"
            raise AppError(message=message)

    def list(self, data: dict):
        try:
            trainers = list(Trainer.objects.all().values("id", "name", "age"))
            for trainer in trainers:
                trainer["team"] = [
                    pokemon
                    for pokemon in Trainer.objects.get(id=trainer["id"])
                    .team.all()
                    .values("id_pokemon", "name", "type")
                ]

            return trainers
        except Exception as e:
            message = f"Error getting trainers list: {e}"
            raise AppError(message=message)


class TeamRepository:
    def create_team(self, data: dict):
        try:
            id = data.pop("id")
            trainer = Trainer.objects.get(id=id)
            trainer.team.set(**data)
            return {
                "team": [pokemon for pokemon in trainer.team.all().values()],
            }
        except Exception as e:
            pass

    def update_team(self, data: dict):
        try:
            trainer = Trainer.objects.get(id=data["id"])
            update_team = data.get("team")
            update_pokemons = []
            for update_pokemon in update_team:
                id_pokemon = update_pokemon["id_pokemon"]
                pokemon = Pokemons.objects.get(id_pokemon=id_pokemon)
                trainer.team.clear()
                update_pokemons.append(pokemon)
            trainer.team.set(update_pokemons)
            trainer.save()
            return {
                "team": [pokemon for pokemon in trainer.team.all().values()],
            }
        except Exception as e:
            message = f"Error updating trainer team: {e}"
            raise AppError(message=message)

    def get_team(self, data: dict):
        try:
            trainer = Trainer.objects.get(id=data["id"])
            return {"team": [pokemon for pokemon in trainer.team.all().values()]}
        except Exception as e:
            message = f"Error getting trainer team: {e}"
            raise AppError(message=message)

    def delete_team(self, data: dict):
        try:
            trainer = Trainer.objects.get(id=data["id"])
            trainer.team.clear()
            return {"message": "Team has been deleted with success"}
        except Exception as e:
            message = f"Error deleting trainer team: {e}"
            raise AppError(message=message)
