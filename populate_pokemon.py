from poke.models import Pokemons
import requests


def populate_pokemon():
    id = 1
    while id <= 10271:
        response = requests.get(f"https://pokeapi.co/api/v2/pokemon/{id}")
        status_code = response.status_code
        if status_code < 400:
            data = response.json()
            pokemon = Pokemons.objects.create(
                name=data["name"],
                weight=data["weight"],
                id_pokemon=data["id"],
                type=data["types"][0]["type"]["name"],
                experience=data["base_experience"],
                picture=f"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/{id}.png",
            )
            pokemon.save()
            print(f"pokemon {id} cadastrado")
        else:
            print(f"nao encontrado o pokemon {id}")

        if id == 1011:
            id = 10000
        id += 1


populate_pokemon()
print("Pokemons Cadatrados")
