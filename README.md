# :rocket: Poke API

<p align="center">
  <a>
    <img src="https://img.shields.io/badge/progress-100%25-brightgreen.svg" alt="progress">
  </a>
  <a>
    <img src="https://img.shields.io/badge/contribuition-welcome-brightgreen.svg" alt="contribution">
  </a>
  <a>
    <img src="https://img.shields.io/badge/version-1.0-brightgreen.svg" alt="version">
  </a>
</p>

## :mag_right: Descrição

-   API de consulta de pokemons e criação de times para cada treinador.

### :hammer_and_wrench: Tecnologias

---

-   Python
-   Docker
-   Django

---

### :heavy_check_mark: Objetivo

---

-   API foi desenvolvida para a execução da dinâmica durante a realização do workshop da equipe CB Lab.
